import Vue from "vue";
import VueRouter from "vue-router";
import Inicial from "./components/Inicial.vue";
import TodosProdutos from "./components/TodosProdutos.vue";
import Entrar from "./components/Entrar.vue";
import Cadastro from "./components/Cadastro.vue"
import Produto from "./components/Produto.vue"
import Busca from "./components/Busca.vue"
import Carrinho from "./components/Carrinho.vue"

Vue.use(VueRouter);
export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Inicial, name: 'inicial' },
    { path: "/todosprodutos", component: TodosProdutos },
    { path: "/entrar", component: Entrar },
    { path: "/cadastro", component: Cadastro },
    { path: "/produtos/:id", component: Produto },
    { path: "/busca/:produto", component: Busca, name: 'buscar' },
    { path: "/carrinho", component: Carrinho, name: 'carrinho' },

  ],
});
